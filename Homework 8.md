```
 █████╗ ████████╗ █████╗ ██╗      █████╗ ██╗███╗   ██╗
██╔══██╗╚══██╔══╝██╔══██╗██║     ██╔══██╗██║████╗  ██║
███████║   ██║   ███████║██║     ███████║██║██╔██╗ ██║
██╔══██║   ██║   ██╔══██║██║     ██╔══██║██║██║╚██╗██║
██║  ██║   ██║   ██║  ██║███████╗██║  ██║██║██║ ╚████║
╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝
```
# Настройка PostgreSQL 

## Домашняя работа

1) Развернуть виртуальную машину любым удобным способом

Развернул виртуальную машину на своем сервере Proxmox

Конфиг виртуальной машины

| Параметр | Характеристики |  
|----------|---------------------------------|
|CPU |8|
|RAM|16ГБ|
|SSD| 30ГБ|
| Тип SSD | nvme |


2) Поставить на неё PostgreSQL 15 любым способом

Так как я использовал шаблон с прошлых работ, то PostgreSQL 15 стоит. Создал новый кластер.

![Alt text](img/image-108.png)

3) Настроить кластер PostgreSQL 15 на максимальную производительность не
обращая внимание на возможные проблемы с надежностью в случае
аварийной перезагрузки виртуальной машины

| Настройки | Значение по умолчанию  | Моя настройки | 
|----------|---------------------------------|----------------------------------|
|shared_buffers|128 MB|4GB|
|max_connections|100|53|
|effective_cache_size| |12GB|
|maintenance_work_mem| |1GB|
|work_mem| |19784kB|
|wal_buffers|-1 |16 MB|
|min_wal_size|80 MB|1 GB|
|max_wal_size|1 GB|4 GB|
|checkpoint_timeout|5 min|1H|
|effective_io_concurrency| |200|
|default_statistics_target| | 100|
|max_worker_processes | | 8|
|max_parallel_workers_per_gather| |4|
|max_parallel_workers| |8|
|max_parallel_maintenance_workers| |4|
|autovacuum| on |off|
|wal_level| replica |minimal|
|max_wal_senders|10 |0|
|synchronous_commit| on | off|
|fsync| on |off|
|full_page_writes| on|off|

4) Нагрузить кластер через утилиту pgbench (https://postgrespro.ru/docs/postgrespro/14/pgbench)

Создал отдельную бд для теста

![Alt text](img/image-109.png)

Провел тест при стандартных настройках PostgreSQL

![Alt text](img/image-110.png)

Провел тест с моими настройками

![Alt text](img/image-111.png)

5) Написать какого значения tps удалось достичь, показать какие параметры в
какие значения устанавливали и почему

На настройках поумолчанию удалось достичь tps = 1021, а при моих настройках tps составил 4785. В целом результатом очень доволен, увеличился tps в 4.68 раз (но на проде жаль, такое не провернешь), единственное можно было улучшить результат используя pgbouncer и [включением huge pages](https://www.percona.com/blog/why-linux-hugepages-are-super-important-for-database-servers-a-case-with-postgresql/) но есть проблемы с этом в моем типе виртуализации. 

Сделал все, чтобы меньше было информации в wal и как можно реже он попадал на диск + асинхронный режим включил - это по настройкам full_page_writes, fsync, synchronous_commit, max_wal_senders, wal_level, max_wal_size, min_wal_size, wal_buffers, checkpoint_timeout.

Отключил autovacuum, чтобы воркеры не отбирали ресурсов.

Так как у меня ssd effective_io_concurrency и default_statistics_target сделал настройки рекомендованные под ссд.

Так же задал рекомендованные настройки по формулам которые можно найти в любом гайде и туториале. shared_buffers,effective_cache_size, maintenance_work_mem,work_mem, max_worker_processes,max_parallel_workers_per_gather, max_parallel_workers,max_parallel_maintenance_workers.

Max_connections поставил 53, так как боялся что тест упадет при 50 клиентах, решил что 3 на запас хватит.

P.S. Тестил на 50 коннектов tps вырос до 4970.