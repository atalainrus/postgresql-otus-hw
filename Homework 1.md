```
 █████╗ ████████╗ █████╗ ██╗      █████╗ ██╗███╗   ██╗
██╔══██╗╚══██╔══╝██╔══██╗██║     ██╔══██╗██║████╗  ██║
███████║   ██║   ███████║██║     ███████║██║██╔██╗ ██║
██╔══██║   ██║   ██╔══██║██║     ██╔══██║██║██║╚██╗██║
██║  ██║   ██║   ██║  ██║███████╗██║  ██║██║██║ ╚████║
╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝
```
# SQL и реляционные СУБД. Введение в PostgreSQL 

## Домашняя работа
1. Cоздать новый проект в Google Cloud Platform, Яндекс облако или на любых ВМ, докере. Далее создать инстанс виртуальной машины с дефолтными параметрами
Создал контейнер Ubuntu 22.04 на своем сервере на базе Proxmox, назвал otus-postgres

![Alt text](img/image.png)
1. Добавить свой ssh ключ в metadata ВМ
Создал пользователя atalain и добавил ssh ключ.

![Alt text](img/image-1.png)
1. Зайти удаленным ssh (первая сессия), не забывайте про ssh-add

![Alt text](img/image-2.png)
1. Поставить PostgreSQL
Добавил репозиторий PostgreSQL и установил PostgreSQL 15 на виртуальную машину

![Alt text](img/image-3.png)
1. Зайти вторым ssh (вторая сессия). Запустить везде psql из под пользователя postgres.

![Alt text](img/image-4.png)
1. Выключить auto commit

![Alt text](img/image-5.png)
1. Сделать в первой сессии новую таблицу и наполнить ее данными create table persons(id serial, first_name text, second_name text); insert into persons(first_name, second_name) values('ivan', 'ivanov'); insert into persons(first_name, second_name) values('petr', 'petrov'); commit;

![Alt text](img/image-6.png)
1. Посмотреть текущий уровень изоляции: show transaction isolation level

![Alt text](img/image-7.png)
1. Начать новую транзакцию в обоих сессиях с дефолтным (не меняя) уровнем изоляции.
В первой сессии добавить новую запись insert into persons(first_name, second_name) values('sergey', 'sergeev'); Сделать select * from persons во второй сессии. Видите ли вы новую запись и если да то почему?

![Alt text](img/image-8.png)
Записи не видно, так нет грязного чтения.
1. Завершить первую транзакцию - commit; сделать select * from persons во второй сессии. Видите ли вы новую запись и если да то почему?

![Alt text](img/image-9.png)
Запись видно, так как транзакцию с insert подтвердили и воспроизвели аномалию неповторяющегося чтения. 
1. Завершите транзакцию во второй сессии. Начать новые но уже repeatable read транзакции - set transaction isolation level repeatable read;

![Alt text](img/image-10.png)
1. В первой сессии добавить новую запись insert into persons(first_name, second_name) values('sveta', 'svetova'); Сделать select * from persons во второй сессии. Видите ли вы новую запись и если да то почему?

![Alt text](img/image-11.png)
Записи не видно, так как на уровне repeatable read не допускается грязного чтения.
1. Завершить первую транзакцию - commit; Сделать select * from persons во второй сессии. Видите ли вы новую запись и если да то почему?

![Alt text](img/image-12.png)
Записи не видно, так как на уровне repeatable read в PostgreSQL не допускается неповторяющегося чтения.
1. Завершить вторую транзакцию, сделать select * from persons во второй сессии. Видите ли вы новую запись и если да то почему?

![Alt text](img/image-13.png)

Запись видно, так как все транзакции зафиксированы.