```
 █████╗ ████████╗ █████╗ ██╗      █████╗ ██╗███╗   ██╗
██╔══██╗╚══██╔══╝██╔══██╗██║     ██╔══██╗██║████╗  ██║
███████║   ██║   ███████║██║     ███████║██║██╔██╗ ██║
██╔══██║   ██║   ██╔══██║██║     ██╔══██║██║██║╚██╗██║
██║  ██║   ██║   ██║  ██║███████╗██║  ██║██║██║ ╚████║
╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝
```
# Журналы

## Домашняя работа

1) Настройте выполнение контрольной точки раз в 30 секунд.

Создал новый кластер PostgreSQL

![Alt text](img/image-97.png)

![Alt text](img/image-96.png)

2) 10 минут c помощью утилиты pgbench подавайте нагрузку.

Провел тест

![Alt text](img/image-98.png)

3) Измерьте, какой объем журнальных файлов был сгенерирован за это время. Оцените, какой объем приходится в среднем на одну контрольную точку.

Замерил размер директории wal до теста

![Alt text](img/image-99.png)

Замерил размер директории wal после теста

![Alt text](img/image-100.png)

За время теста сгенерировано было 81 - 17 = 64 мб.
За каждую контрольную точку было в среднем сгенерировано 64 / (600 / 30) = 3.2 мб за контрольную точку

4) Проверьте данные статистики: все ли контрольные точки выполнялись точно по расписанию. Почему так произошло?

Судя по статистике логам, контрольные точки выполнялись примерно каждые ~27 секунд и каждая последующая контрольная точка начиналась через ~3 секунды.В целом все в норме, но при еще большей нагрузке или медленном диске, точки могли уже накладываться друг на друга.

![Alt text](img/image-101.png)

5) Сравните tps в синхронном/асинхронном режиме утилитой pgbench. Объясните полученный результат.

Проверяю режим работы

![Alt text](img/image-102.png)

Запустил тест синхронным режиме работы

![Alt text](img/image-103.png)

Включаю асинхронный режим работы

![Alt text](img/image-104.png)

Запускаю тест в асинхронным режиме работы

![Alt text](img/image-105.png)

Существенно вырос tps в асинхронном режиме работы 3301.296021 против 1226.503250 в с синхронным режиме работы, прирост составил ~169%. Это произошло потому что в асинхронном режиме в отличии от синхронного режима сервер сообщает об успешном завершении сразу, как только транзакция будет завершена логически, прежде чем сгенерированные записи WAL фактически будут записаны на диск. Асинхронная фиксация даёт возможность завершать транзакции быстрее, ценой того, что в случае краха СУБД последние транзакции могут быть потеряны. 

6) Создайте новый кластер с включенной контрольной суммой страниц. Создайте таблицу. Вставьте несколько значений. Выключите кластер. Измените пару байт в таблице. Включите кластер и сделайте выборку из таблицы. Что и почему произошло? 

Создал кластер с контрольной суммой страниц и проверил, что все ок.

![Alt text](img/image-106.png)

Совершил все действия как в указано в задании

![Alt text](img/image-107.png)

При попытке вывести данные из таблице, падает ошибка, что данные повреждены. После того как мы изменили пару байт, чек суммы не сходятся.

7) Как проигнорировать ошибку и продолжить работу?

1. Использовать параметр ignore_checksum_failure = on для того чтобы считать целые данные.
2. Найти и удалить поврежденные строки, если такое не критично.
3. Восстановить поврежденные строки из бэкапа.


