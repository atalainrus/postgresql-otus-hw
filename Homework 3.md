```
 █████╗ ████████╗ █████╗ ██╗      █████╗ ██╗███╗   ██╗
██╔══██╗╚══██╔══╝██╔══██╗██║     ██╔══██╗██║████╗  ██║
███████║   ██║   ███████║██║     ███████║██║██╔██╗ ██║
██╔══██║   ██║   ██╔══██║██║     ██╔══██║██║██║╚██╗██║
██║  ██║   ██║   ██║  ██║███████╗██║  ██║██║██║ ╚████║
╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝
```
# Физический уровень PostgreSQL 

## Домашняя работа

1. Создайте виртуальную машину c Ubuntu 20.04/22.04 LTS в GCE/ЯО/Virtual Box/докере. Поставьте на нее PostgreSQL 15 через sudo apt
Развернул виртуальную машину с Ubuntu 22.04 на своем сервере на базе Proxmox, назвал otus-postgres установил PostgreSQL 15 

![Alt text](img/image-22.png)
1. Проверьте что кластер запущен через sudo -u postgres pg_lsclusters

![Alt text](img/image-23.png)
1. Зайдите из под пользователя postgres в psql и сделайте произвольную таблицу с произвольным содержимым
postgres=# create table test(c1 text);
postgres=# insert into test values('1');
\q

![Alt text](img/image-24.png)
1. Остановите postgres например через sudo -u postgres pg_ctlcluster 15 main stop

![Alt text](img/image-25.png)
Остановил postgresql
1. Создайте новый диск к ВМ размером 10GB. Добавьте свеже-созданный диск к виртуальной машине - надо зайти в режим ее редактирования и дальше выбрать пункт attach existing disk

![Alt text](img/image-26.png)
Добавил к виртуальной машине диск 10 гб.
1. Проинициализируйте диск согласно инструкции и подмонтировать файловую систему, только не забывайте менять имя диска на актуальное, в вашем случае это скорее всего будет /dev/sdb - https://www.digitalocean.com/community/tutorials/how-to-partition-and-format-storage-devices-in-linux
Перезагрузите инстанс и убедитесь, что диск остается примонтированным (если не так смотрим в сторону fstab)

![Alt text](img/image-27.png)
Примонтировал диск в /mnt/data, после перезагрузки он примонтировался.
1. Сделайте пользователя postgres владельцем /mnt/data - chown -R postgres:postgres /mnt/data/

![Alt text](img/image-28.png)
Сменил владельца
1. Перенесите содержимое /var/lib/postgres/15 в /mnt/data - mv /var/lib/postgresql/15/mnt/data

![Alt text](img/image-29.png)
Переместил данные из директории /var/lib/postgres/15 в /mnt/data
1. Попытайтесь запустить кластер - sudo -u postgres pg_ctlcluster 15 main start
напишите получилось или нет и почему

![Alt text](img/image-30.png)
Попытался запустить, но не вышло, так как пунктом выше я перенес все файлы PostgreSQL в новую директорию, а в конфиге не поменял путь до них (data directory).
1. Задание: найти конфигурационный параметр в файлах расположенных в /etc/postgresql/15/main который надо поменять и поменяйте его
напишите что и почему поменяли

![Alt text](img/image-31.png)
В конфигурационном файле /etc/postgresql/15/main/postgresql.conf изменил параметр data directory на новый путь до файлов PostgreSQL.
1. Попытайтесь запустить кластер - sudo -u postgres pg_ctlcluster 15 main start
напишите получилось или нет и почему
Получилось запустить так как новый путь до data directory теперь верный.
1. Зайдите через через psql и проверьте содержимое ранее созданной таблицы

![Alt text](img/image-32.png)
Подключился к серверу и проверил, что данные сохранены.
1. Задание со звездочкой *: не удаляя существующий инстанс ВМ сделайте новый, поставьте на его PostgreSQL, удалите файлы с данными из /var/lib/postgres, перемонтируйте внешний диск который сделали ранее от первой виртуальной машины ко второй и запустите PostgreSQL на второй машине так чтобы он работал с данными на внешнем диске, расскажите как вы это сделали и что в итоге получилось.

Поднял вторую ВМ и PostgreSQL, удалил на ней данные postgresql (в теории можно было не удалять, так как nfs перемонтировал бы директорию).

![Alt text](img/image-33.png)
Не делал такого прежде, но выбор пал на NFS для расшаривания сети + wiki PostgreSQL.
На первой ВМ установил nfs server
>sudo apt install nfs-kernel-server

![Alt text](img/image-34.png)
Добавил в конфиг данные для расшаривания директории с Postgresql и перезагрузил службу nfs

![Alt text](img/image-35.png)

Установил client NFS на вторую ВМ

![Alt text](img/image-36.png)

Примонтировал NFS на вторую ВМ

![Alt text](img/image-37.png)

Запустил кластер PostgreSQL на второй ВМ

![Alt text](img/image-38.png)

Подключился к PostgreSQL с второй ВМ и проверил, что данные на месте

![Alt text](img/image-39.png)