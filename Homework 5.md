```
 █████╗ ████████╗ █████╗ ██╗      █████╗ ██╗███╗   ██╗
██╔══██╗╚══██╔══╝██╔══██╗██║     ██╔══██╗██║████╗  ██║
███████║   ██║   ███████║██║     ███████║██║██╔██╗ ██║
██╔══██║   ██║   ██╔══██║██║     ██╔══██║██║██║╚██╗██║
██║  ██║   ██║   ██║  ██║███████╗██║  ██║██║██║ ╚████║
╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝
```
# MVCC, vacuum и autovacuum 

## Домашняя работа

1. Создать инстанс ВМ с 2 ядрами и 4 Гб ОЗУ и SSD 10GB

Развернул из своего шаблона инстанс с 2 ядрами и 4 Гб ОЗУ и SSD 30GB (SSD больше думаю не критично)

![Alt text](img/image-61.png)

2. Установить на него PostgreSQL 15 с дефолтными настройками

Поднял кластер с дефолтными настройками

![Alt text](img/image-62.png)

3. Создать БД для тестов: выполнить pgbench -i postgres

Создал бд для тестов pgbench

![Alt text](img/image-63.png)

И выполнил pgbench -i pgbench

![Alt text](img/image-64.png)

4. Запустить pgbench -c8 -P 6 -T 60 -U postgres postgres

Запустил pgbench -c8 -P 6 -T 60 -U postgres pgbench

![Alt text](img/image-65.png)


5. Применить параметры настройки PostgreSQL из прикрепленного к материалам занятия файла

Применил настройки

![Alt text](img/image-66.png)

6. Протестировать заново. Что изменилось и почему?

![Alt text](img/image-67.png)

Скорости работы СУБД стала не значительно быстрее, но скорей всего из-за того что в тесты синтетические и про правильно настройке на реальной базе данных можно получить значительный прирост производительности и тонко настроить СУБД под тип БД (oltp, dwh) так как СУБД будет более правильно расходовать ресурсы сервера. 

7. Создать таблицу с текстовым полем и заполнить случайными или сгенерированными данным в размере 1млн строк

![Alt text](img/image-68.png)

8. Посмотреть размер файла с таблицей

![Alt text](img/image-69.png)

9. 5 раз обновить все строчки и добавить к каждой строчке любой символ

![Alt text](img/image-70.png)

10. Посмотреть количество мертвых строчек в таблице и когда последний раз приходил автовакуум


![Alt text](img/image-71.png)

11. Подождать некоторое время, проверяя, пришел ли автовакуум

![Alt text](img/image-72.png)

12. 5 раз обновить все строчки и добавить к каждой строчке любой символ

![Alt text](img/image-73.png)

13. Посмотреть размер файла с таблицей

![Alt text](img/image-74.png)

14. Отключить Автовакуум на конкретной таблице

![Alt text](img/image-75.png)

15. 10 раз обновить все строчки и добавить к каждой строчке любой символ

![Alt text](img/image-76.png)

16. Посмотреть размер файла с таблицей

![Alt text](img/image-77.png)

17. Объясните полученный результат. Не забудьте включить автовакуум)

Так как update не изменяет данные поверх старых, а старые строки помечает как удаленные, а новые данные записывает в таблицу как новые строчки. AUTOVACUUM чистит помеченные строчки на удаление и в образовавшиеся пробелы при следующем обновлении и добавлении данных добавляются данные, поэтому таблица не так сильно растет как при включенном AUTOVACUUM. После 10 update при включонном AUTOVACUUM в таблице было 3 млн строк. При выключенном AUTOVACUUM в таблице было 11 млн строк.

![Alt text](img/image-78.png)

Для уменьшения размера необходимо запустить VACUUM FULL.

![Alt text](img/image-79.png)

Проверяем - размер уменьшился.

![Alt text](img/image-80.png)

18. Задание со *:
Написать анонимную процедуру, в которой в цикле 10 раз обновятся все строчки в искомой таблице.
Не забыть вывести номер шага цикла. 

![Alt text](img/image-81.png)

![Alt text](img/image-82.png)

